## Para instalar as dependências, execute:

### `yarn` ou `npm install`

## Para iniciar o projeto, execute:

### `yarn start` ou `npm run start`

## Para definir a URL da api
- Crie a variável de ambiente `REACT_APP_BASE_URL`

Este projeto utiliza como back-end o seguinte projeto:
[Api de back-end](https://gitlab.com/jairochaves/affero-api-produtos)
