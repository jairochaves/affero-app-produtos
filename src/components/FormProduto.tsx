import { Button, Grid, TextField } from "@material-ui/core";
import React from "react";
import Product from "../interfaces/Product.interface";

interface Props {
  product: Product | undefined;
  onChange: (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => void;
  saveHandle: () => Promise<void>;
}
function FormProduto({
  product,
  onChange,
  saveHandle,
}: Props): React.ReactElement<Props> {
  const [emptyCheck, setEmptyCheck] = React.useState(false);
  const save = async () => {
    if (
      product?.categoria &&
      product?.codigoBarras &&
      product?.descricao &&
      product?.nome &&
      product?.quantidade
    ) {
      await saveHandle();
    } else {
      setEmptyCheck(true);
    }
  };
  return (
    <React.Fragment>
      <Grid item xs={12} md={8}>
        <TextField
          required
          fullWidth
          id="nome"
          label="Nome"
          name="nome"
          value={product?.nome}
          onChange={onChange}
          error={emptyCheck && !product?.nome}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <TextField
          required
          fullWidth
          id="quantidade"
          label="Quantidade"
          name="quantidade"
          type="number"
          value={product?.quantidade}
          onChange={onChange}
          error={emptyCheck && !product?.quantidade}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          required
          fullWidth
          id="descricao"
          label="Descrição"
          name="descricao"
          multiline
          rows={2}
          value={product?.descricao}
          onChange={onChange}
          error={emptyCheck && !product?.descricao}
        />
      </Grid>

      <Grid item xs={12} md={6}>
        <TextField
          required
          fullWidth
          id="categoria"
          label="Categoria"
          name="categoria"
          value={product?.categoria}
          onChange={onChange}
          error={emptyCheck && !product?.categoria}
        />
      </Grid>
      <Grid item xs={12} md={6}>
        <TextField
          required
          fullWidth
          id="codigoBarras"
          label="Código de Barras"
          name="codigoBarras"
          value={product?.codigoBarras}
          onChange={onChange}
          error={emptyCheck && !product?.codigoBarras}
        />
      </Grid>
      <Grid item xs={12}>
        <Button fullWidth onClick={save} variant="contained" type="submit">
          Salvar
        </Button>
      </Grid>
    </React.Fragment>
  );
}

export default FormProduto;
