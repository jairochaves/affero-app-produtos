import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Paper,
} from "@material-ui/core";
import React from "react";
import Product from "../interfaces/Product.interface";
import api from "../services/api";
import FormProduto from "./FormProduto";
import { useProducts } from "./../contexts/AllProducts.context";

interface Props {
  product: Product | undefined;
  open: boolean;
  close: () => void;
}
const DialogUpdate: React.FC<Props> = ({ open, close, product }) => {
  const [productUpdate, setProduct] = React.useState(product);
  const { reloadProducts } = useProducts();

  React.useEffect(() => {
    setProduct(product);
  }, [product]);

  const onChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    if (productUpdate) {
      let produto = {
        ...productUpdate,
        [event.target.name]: event.target.value,
      };
      setProduct(produto);
    }
  };

  const updateHandle = async () => {
    api.put(`produtos/${product?.id}`, productUpdate).then((response) => {
      close();
      reloadProducts();
    });
  };
  return (
    <Dialog open={open} onClose={close}>
      <DialogTitle sx={{ bgcolor: "primary.main", color: "white" }}>
        Atualizar Produto
      </DialogTitle>
      <DialogContent sx={{ mt: 2 }}>
        <Paper sx={{ mt: 1, p: 4, ml: 1 }}>
          <Grid container spacing={2}>
            <FormProduto
              onChange={onChange}
              saveHandle={updateHandle}
              product={productUpdate}
            />
          </Grid>
        </Paper>
      </DialogContent>
    </Dialog>
  );
};

export default DialogUpdate;
