import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import React from "react";
import { useProducts } from "../contexts/AllProducts.context";
import Product from "../interfaces/Product.interface";
import api from "../services/api";
interface Props {
  product: Product | undefined;
  open: boolean;
  close: () => void;
}
const DialogDelete: React.FC<Props> = ({ open, close, product }) => {
  const { reloadProducts } = useProducts();

  const deleteProduct = async (id: number): Promise<void> => {
    await api
      .delete(`produtos/${id}`)
      .then(() => {
        reloadProducts();
      })
      .catch((e) => console.log(e));
  };

  return (
    <Dialog open={open} onClose={close}>
      <DialogTitle sx={{ bgcolor: "error.main", color: "white" }}>
        Deseja remover este produto?
      </DialogTitle>
      <DialogContent sx={{ mt: 2 }}>
        <Grid container>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText primary="ID" secondary={product?.id} />
            </ListItem>
          </Grid>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText primary="Nome" secondary={product?.nome} />
            </ListItem>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button variant="text" onClick={close}>
          Cancelar
        </Button>
        <Button
          onClick={() => {
            if (product?.id) {
              deleteProduct(product.id);
              close();
            }
          }}
          variant="contained"
          color="error"
        >
          Remover
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogDelete;
