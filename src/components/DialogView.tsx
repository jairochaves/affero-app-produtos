import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import React from "react";
import Product from "../interfaces/Product.interface";
interface Props {
  product: Product | undefined;
  open: boolean;
  close: () => void;
}
const DialogView: React.FC<Props> = ({ open, close, product }) => {
  return (
    <Dialog maxWidth="sm" fullWidth open={open} onClose={close}>
      <DialogTitle sx={{ bgcolor: "success.main", color: "white" }}>
        {product?.nome}
      </DialogTitle>
      <DialogContent sx={{ mt: 2 }}>
        <Grid container>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText primary="ID" secondary={product?.id} />
            </ListItem>
          </Grid>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText primary="Nome" secondary={product?.nome} />
            </ListItem>
          </Grid>
          <Grid item xs={12}>
            <ListItem divider>
              <ListItemText
                primary="Descrição"
                secondary={product?.descricao}
              />
            </ListItem>
          </Grid>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText
                primary="Código de Barras"
                secondary={product?.codigoBarras}
              />
            </ListItem>
          </Grid>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText
                primary="Quantidade"
                secondary={product?.quantidade}
              />
            </ListItem>
          </Grid>
          <Grid item xs={12} md={6}>
            <ListItem divider>
              <ListItemText
                primary="Categoria"
                secondary={product?.categoria}
              />
            </ListItem>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};

export default DialogView;
