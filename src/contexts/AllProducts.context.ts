import { createContext, useContext } from "react";
import Product from "../interfaces/Product.interface";

interface ContextType {
  products: Product[];

  reloadProducts: () => void;
}
const allProductsContext = createContext<ContextType>({
  products: [],

  reloadProducts: () => {},
});

export function useProducts(): ContextType {
  const context = useContext(allProductsContext);
  return context;
}
export default allProductsContext;
