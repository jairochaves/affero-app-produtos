import React, { useEffect } from "react";
import { useProducts } from "../contexts/AllProducts.context";
import { DataGrid } from "@mui/x-data-grid";

export default function Produtos() {
  const { products } = useProducts();
  useEffect(() => {}, []);

  return (
    <div style={{ flex: 1 }}>
      <DataGrid
        columns={[
          { field: "id", headerName: "ID", width: 75, align: "center" },
          { field: "nome", headerName: "Nome", flex: 1 },
          { field: "descricao", headerName: "Descricao", flex: 1 },
          { field: "codigoBarras", headerName: "Código de Barras", flex: 1 },
          { field: "quantidade", headerName: "Quantidade", flex: 1 },
          { field: "categoria", headerName: "Categoria", flex: 1 },
        ]}
        rows={products}
      />
    </div>
  );
}
