import { Grid, Paper } from "@material-ui/core";
import { Alert, Snackbar } from "@mui/material";
import React from "react";
import FormProduto from "../components/FormProduto";
import { useProducts } from "../contexts/AllProducts.context";
import Product from "../interfaces/Product.interface";
import api from "../services/api";

const INITIAL_PRODUCT = {
  categoria: "",
  nome: "",
  codigoBarras: "",
  descricao: "",
  quantidade: 0,
};

export default function NovoProduto() {
  const { reloadProducts } = useProducts();

  const newProduct = async (product: Product) => {
    return await api.post("produtos", product);
  };

  const [product, setProduct] = React.useState<Product>(INITIAL_PRODUCT);
  const [salvo, setSalvo] = React.useState<boolean>();
  const [errorSave, setErrorSave] = React.useState({
    active: false,
    message: "",
  });

  const saveHandle = async () => {
    newProduct(product)
      .then(() => {
        reloadProducts();
        setSalvo(true);

        setProduct({
          categoria: "",
          nome: "",
          codigoBarras: "",
          descricao: "",
          quantidade: 0,
        });
      })
      .catch(({ response }) => {
        if (response.data.errors) {
          response.data.errors.foreach((error: any) => {
            setErrorSave({
              active: true,
              message: `Campo: ${error.field} | Mensagem: ${error.message}`,
            });
          });
        }
      });
  };
  const onChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    let produto = { ...product, [event.target.name]: event.target.value };
    setProduct(produto);
  };

  return (
    <Paper sx={{ mt: 1, p: 4, ml: 1 }}>
      <Grid container spacing={2}>
        <FormProduto
          onChange={onChange}
          saveHandle={saveHandle}
          product={product}
        />
      </Grid>
      <Snackbar
        open={salvo || errorSave.active}
        // autoHideDuration={6000}

        onClose={() => {
          setSalvo(false);
          setErrorSave({ active: false, message: "" });
        }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <Alert
          variant="filled"
          severity={salvo ? "success" : "error"}
          sx={{ width: "100%" }}
          onClose={() => {
            setSalvo(false);
            setErrorSave({ active: false, message: "" });
          }}
        >
          {salvo ? "Produto Salvo!" : errorSave.message}
        </Alert>
      </Snackbar>
    </Paper>
  );
}
