import React from "react";
import {
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons";
import { ListItemIcon } from "@mui/material";
import { useProducts } from "../contexts/AllProducts.context";
import { Info } from "@mui/icons-material";
import DialogDelete from "../components/DialogDelete";
import Product from "../interfaces/Product.interface";
import DialogView from "../components/DialogView";
import DialogUpdate from "../components/DialogUpdate";

export default function AcoesProduto() {
  const { products } = useProducts();
  const [openDelete, setOpenDelete] = React.useState(false);
  const [openView, setOpenView] = React.useState(false);
  const [openUpdate, setOpenUpdate] = React.useState(false);

  const [selectedProduct, setSelectedProduct] = React.useState<Product>();
  return (
    <div style={{ flex: 1, marginTop: 4 }}>
      <List sx={{ bgcolor: "background.paper" }}>
        {products.map((produto) => {
          return (
            <ListItem key={produto.id}>
              <ListItemAvatar>{produto.id}</ListItemAvatar>
              <ListItemText
                primary={produto.nome}
                secondary={produto.descricao}
              />
              <ListItemIcon>
                <IconButton
                  onClick={() => {
                    setOpenView(true);
                    setSelectedProduct(produto);
                  }}
                  color="success"
                  title="Detalhes"
                >
                  <Info />
                </IconButton>
                <IconButton
                  onClick={() => {
                    setOpenUpdate(true);
                    setSelectedProduct(produto);
                  }}
                  color="info"
                  title="Editar"
                >
                  <Edit />
                </IconButton>
                <IconButton
                  onClick={() => {
                    setOpenDelete(true);
                    setSelectedProduct(produto);
                  }}
                  color="error"
                  title="Remover"
                >
                  <Delete />
                </IconButton>
              </ListItemIcon>
            </ListItem>
          );
        })}
      </List>
      <DialogDelete
        open={openDelete}
        product={selectedProduct}
        close={() => {
          setOpenDelete(false);
        }}
      />
      <DialogView
        open={openView}
        product={selectedProduct}
        close={() => {
          setOpenView(false);
        }}
      />
      <DialogUpdate
        open={openUpdate}
        product={selectedProduct}
        close={() => {
          setOpenUpdate(false);
        }}
      />
    </div>
  );
}
