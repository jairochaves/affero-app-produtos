import React from "react";
import camelCase from "camelcase-keys";
import AllProductsContext from "../contexts/AllProducts.context";
import Product from "../interfaces/Product.interface";
import api from "../services/api";

const AllProducts: React.FC = ({ children }) => {
  const [products, setProducts] = React.useState<Product[]>([]);
  const [updateProducts, setUpdateProducts] = React.useState<boolean>(false);
  React.useEffect(() => {
    api
      .get("produtos")
      .then(({ data }) => {
        setProducts(camelCase(data));
      })
      .catch((e) => console.log({ e }));
  }, [updateProducts]);

  const reloadProducts = () => {
    setUpdateProducts(!updateProducts);
  };
  return (
    <AllProductsContext.Provider value={{ products, reloadProducts }}>
      {children}
    </AllProductsContext.Provider>
  );
};

export default AllProducts;
