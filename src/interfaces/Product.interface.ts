export default interface Product {
  id?: number;
  nome: string;
  codigoBarras: string;
  descricao: string;
  quantidade?: number;
  categoria: string;
}
