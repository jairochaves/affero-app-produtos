import {
  BottomNavigation,
  BottomNavigationAction,
  Container,
  Paper,
} from "@material-ui/core";
import { AddCircle, ListAlt } from "@material-ui/icons";
import React from "react";
import "./assets/css/pages.css";
import AcoesProduto from "./pages/AcoesProduto";
import NovoProduto from "./pages/NovoProduto";
import AllProducts from "./providers/AllProducts.provider";

function App() {
  const [value, setValue] = React.useState<number>(0);
  return (
    <AllProducts>
      <Container
        maxWidth="xl"
        sx={{
          height: "100vh",
          pt: 1,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Paper elevation={3}>
          <BottomNavigation
            showLabels
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
          >
            <BottomNavigationAction label="Produtos" icon={<ListAlt />} />
            <BottomNavigationAction
              label="Cadastrar Produto"
              icon={<AddCircle />}
              // <BottomNavigationAction label="Ações" icon={<ConstructionIcon />} />
            />
          </BottomNavigation>
        </Paper>
        <div
          className="root_page"
          style={{
            flex: 1,
            display: "flex",
            flexDirection: "column",
            overflow: "scroll",
          }}
        >
          {/* {value === 0 && <Produtos />} */}
          {value === 0 && <AcoesProduto />}
          {value === 1 && <NovoProduto />}
        </div>
      </Container>
    </AllProducts>
  );
}

export default App;
